$(function() {

    $('[data-toggle="tooltip"]').tooltip()

    $('[data-toggle="popover"]').popover()

    $('.carousel').carousel({
        interval: 3000 //3000 milisegundos equivalen a 3 segundos.
    })

    var buttonid = null;


    $('#model-reservation').on('show.bs.modal', function(e) {
        console.log("El modal se esta preparando")
        console.log(e)

        buttonid = e.relatedTarget.id;

        $('#' + buttonid).css("background-color", "#000000");
        $('#' + buttonid).prop('disabled', true);

    })

    $('#model-reservation').on('shown.bs.modal', function(e) {
        console.log("El modal esta listo")

    })

    $('#model-reservation').on('hide.bs.modal', function(e) {
        console.log("El modal se esta llendo")
    })

    $('#model-reservation').on('hidden.bs.modal', function(e) {
        console.log("El modal se fue para siempre")

        $('#' + buttonid).css("background-color", "#480a76");
        $('#' + buttonid).prop('disabled', false);
        buttonid = null;
    })
})